import argparse
import datetime
import traceback

import wmfdata

def valid_args(args):
    """Check if command-line date arguments are valid."""
    is_valid = True

    try:
        datetime.datetime(year=args.year, month=args.month, day=args.day)
    except Exception:
        traceback.print_exc()
        is_valid = False

    print(f"Arguments: {args}")
    return is_valid


def create_hive_trace_table(table='isaacj_group.wprov_daily_source'):
    """Create a table for wprov counts partitioned by day.

    NOTES:
      * wprov, wiki_db, and view_count are the important fields
      * http_status and agent_type are more relevant to checking how the wprov values are being handled
      given the known bug with mobile redirects: https://phabricator.wikimedia.org/T252227
    """
    
    database, tablename = table.split('.')
    # Database must also be created with analytics-privatedata (in same way crontab + shell script are run)
    # so permissions are set appropriately.
    dbquery = f"""
    CREATE DATABASE IF NOT EXISTS {database}
    """
    wmfdata.hive.run(dbquery)

    tbquery = f"""
    CREATE TABLE IF NOT EXISTS {database}.{tablename} (
        wprov                           STRING         COMMENT 'Provenance parameter',
        wiki_db                         STRING         COMMENT 'Project',
        http_status                     STRING         COMMENT 'HTTP status code',
        agent_type                      STRING         COMMENT 'Agent accessing the pages, can be spider, user or automated',
        view_count                      BIGINT         COMMENT 'Number of pageviews referred'
    ) 
    PARTITIONED BY (year INT, month INT, day INT)
    STORED AS PARQUET
    """

    wmfdata.hive.run(tbquery)


def add_day_to_hive_table(year, month, day, table='isaacj_group.wprov_daily_source'):
    """Add one day's worth of data to the Hive table.

    Notes:
      * I filter out obviously invalid wprov values (it seems this happens not infrequently) via RLIKE clause
      * We can't limit to pageview_actor or is_pageview because many wprov values are only on redirects
      that are not counted as pageviews. See: https://phabricator.wikimedia.org/T252227
    """

    query = f"""
    WITH wprov_views AS (
        SELECT
          x_analytics_map['wprov'] AS wprov,
          CONCAT(normalized_host.project, '.', normalized_host.project_family) AS wiki_db,
          CONCAT(http_status, '-', is_pageview) AS http_status,
          agent_type
        FROM wmf.webrequest
        WHERE
          year = {year} AND month = {month} AND day = {day}
          AND webrequest_source = 'text'
          AND x_analytics_map['wprov'] IS NOT NULL
    )
    INSERT OVERWRITE TABLE {table}
    PARTITION(year={year}, month={month}, day={day})
    SELECT
      wprov,
      wiki_db,
      http_status,
      agent_type,
      COUNT(1) AS view_count
    FROM wprov_views
    WHERE
      wprov RLIKE '^[a-zA-Z0-9_]*$'
    GROUP BY
      wprov,
      wiki_db,
      http_status,
      agent_type
    """

    wmfdata.hive.run(query)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--year", type=int,
                        help="Year for which to gather data -- e.g., 2020")
    parser.add_argument("--month", type=int,
                        help="Month for which to gather data -- e.g., '3' for March")
    parser.add_argument("--day", type=int,
                        help="Day for which to gather data -- e.g., '15' for 15th of the month")
    parser.add_argument("--hive_table", default='isaacj_group.wprov_daily_source',
                        help='Hive table where wprov data will be stored.')
    args = parser.parse_args()

    if valid_args(args):
        # Create table in Hive to store data
        print("\n==Creating Hive table (if it doesn't exist)==")
        create_hive_trace_table(table=args.hive_table)

        # Add day to table
        print(f"\n==Adding data from {args.year}-{args.month}-{args.day}==")
        add_day_to_hive_table(year=args.year, month=args.month, day=args.day,
                              table=args.hive_table)

if __name__ == "__main__":
    main()