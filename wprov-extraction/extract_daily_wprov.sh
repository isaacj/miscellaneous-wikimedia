#!/bin/bash
# this shell script can be run daily with the following line in your crontab:
# 0 13 * * * sudo -u analytics-privatedata kerberos-run-command analytics-privatedata /home/isaacj/plugin/extract_daily_wprov.sh
# See: https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Kerberos#Run_a_command_as_a_system_user?

# activate python3 environment
source conda-analytics-activate base

# make sure web proxy set up so script can reach MediaWiki API
export http_proxy=http://webproxy.eqiad.wmnet:8080
export https_proxy=http://webproxy.eqiad.wmnet:8080

HIVE_TABLE="isaacj_group.wprov_daily_source"
# https://stackoverflow.com/questions/15374752/get-yesterdays-date-in-bash-on-linux-dst-safe
DAY=$(date -d "yesterday 13:00" '+%d')
MONTH=$(date -d "yesterday 13:00" '+%m')
YEAR=$(date -d "yesterday 13:00" '+%Y')

echo "Running pipeline for ${YEAR}-${MONTH}-${DAY}."

python /home/isaacj/plugin/add-wprov-data.py --year "${YEAR}" --month "${MONTH}" --day "${DAY}" --hive_table ${HIVE_TABLE}
